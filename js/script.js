/**

	TODO:
	- Few display bugs with fillTableRecords()
	- Total problem (+1h, wjhy?)
    - Create max hours for client quotation -> project JSON

    - Edit mode: convert hour to timestamp and record
 */

$(function() { 
	// init();
    $("#navbar").load("template/navbar.html");

    // Always load first page
    $("#container").load("template/checkin.html", function() {
        init();
        $("a[data-href='employees']").click(function(){
            $("#container").empty();
        })
    });
})

function init(){
	$.material.init()

	// Mark "active" page on the navbar
    $('a[href="' + this.location.pathname + '"]').parent().addClass('active');

    var employees = $.getEmployees();
    var projects = $.getProjects();
    var typeOfJob = $.getTypeOfJob();
    var projectStorage = "";

    if (JSON.parse(localStorage.getItem("project"))) {
        projectStorage = JSON.parse(localStorage.getItem("project"))
    }



    if (!localStorage.getItem("project")) {
        var project_data = {
            "in_out": 0,
            "date": 0,
            "hour": 0,
            "in": 0,
            "out": 0,
            "employee": "",
            "description": "",
            "project": "",
            "checkout_id": 0,
            "typeOfJob": ""
        }

        localStorage.setItem("project", JSON.stringify(project_data));
    }

    if (page("check-in")) {

        /* Prevent user from quiting the page */
        $(window).on("beforeunload", function(e) {
            return false;
        })
    }

    if (page("records")) {
        projectList = $.getProjects();
        $(".get-csv").click(function() {
            d = new Date();
            date = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate();
            var selectedProject = $("#project option:selected").parent().attr("label")

            if (selectedProject != undefined) {
                selectedProject += "_" + $("#project option:selected").html();
            } else {
                selectedProject = $("#project option:selected").html();

            }

            allRecords = $.getAllRecords();
            allRecords = filterRecords(allRecords, "employee", $("#name option:selected").val())
            allRecords = filterRecords(allRecords, "projectSubID", $("#project option:selected").val())

            JSONToCSV(allRecords, date, true, selectedProject);
            var options = {
                content: "CSV downloaded!", // text of the snackbar
                timeout: 1000 // time in milliseconds after the snackbar autohides, 0 is disabled
            }
            $.snackbar(options);

        })

        $(".delete-data").click(function() {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover these data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if (isConfirm) {
                    $(".get-csv").trigger("click");
                    deleteData(function(){
                    	// fillRecordsTable();
                    });
                    // swal("Deleted!", "These data has been deleted. You automatically downloaded the CSV ", "success");
                    swal({   
                    	title: "Deleted",   
                    	text: "These data has been deleted. You automatically downloaded the CSV",   
                    	type: "success"},
                    	function(){
                    		// fillRecordsTable();
                    	})
                } else {
                    swal("Cancelled", "These data are safe :)", "error");
                }
            });

        })
        var editMode = false;
        $(".edit-mode").click(function() {


            editMode = !editMode;
            if (editMode) {
                $("tr .editable").makeEditable()

                $(".edit-mode").text("Save changes");
        		$(".form-disable-in").prop("disabled", true);
                
                clearInterval(tableInterval);

            } else {
        		$(".edit-mode").removeClass("btn-success").addClass("btn-disabled").append(" <span class='fa fa-spinner fa-spin'></span>")

                tableInterval = setInterval(function(){
                    fillRecordsTable();
                }, 100);

        		setTimeout(function(){
        			$(".edit-mode").removeClass("btn-disabled").addClass("btn-success").empty().append("Edit mode")
        		}, 300)
        		$(".form-disable-in").prop("disabled", false);
                $(".table-date, .table-in, .table-out, .table-desc").attr("contenteditable", "false")
            }
        })
    }

    /*===============================================
    =            Populate forms on ready            =
    ===============================================*/

    /* Employee select */
    $.each(employees, function(i, v) {
        var selected = "";
        if (projectStorage.employee == v.name) {
            selected = "selected";
        }
        $("#name").append("<option value='" + v.name + "' class='name'" + selected + ">" + v.name + "</option>");
    })

    /* Projects select */
    $.each(projects, function(i, optgroups) {
        $("<optgroup/>", {
            "class": "project",
            "label": optgroups.name,
            "data-id": optgroups.id
        }).appendTo("#project")
        $.each(optgroups.subproject, function(i, v) {
            re = "^" + optgroups.id;
            regex = new RegExp(re);
            var matches = v.id.match(regex)
            var selected = "";
            if (projectStorage.projectSubID == v.id) {
                selected = "selected";
            }
            $("[data-id=" + matches[0]).append("<option value='" + v.id + "' " + selected + " data-name='" + v.subname + "'>" + v.subname + "</option>");
        });
    });

    /* Type of job select */
    $.each(typeOfJob, function(i, v) {
        var selected = "";
        if (projectStorage.type == v.type) {
            selected = "selected";
        }
        $("#type-of-job").append("<option value='" + v.type + "' class='type-of-work'" + selected + ">" + v.type + "</option>");
    })

    /* Enable or disable fields */
    if (getInOutValue() == "in") {
        $(".form-disable-in").prop("disabled", true);
    } else {
        $(".form-disable-in").prop("disabled", false);
    }


    /*=================================
    =            Listeners            =
    =================================*/

    /*----------  Change listener  ----------*/
    $(".form-disable-in").on("change", function() {
        // fillRecordsTable();
        LS_setProject();
    })

    /*----------  Click listener  ----------*/
    $(".check-in").click(function(e) {
        e.preventDefault();
        $(".form-disable-in").prop("disabled", true);
        LS_setProject("in");
        // fillRecordsTable();
        // AmIworking();
        // toggleInOutButton(); // Maybe move it in interval
    })

    $(".check-out").click(function(e) {
        e.preventDefault();
        $(".form-disable-in").prop("disabled", false);
        LS_setProject("out");
        $.saveLastCheckOut();
        // fillRecordsTable();
    })

    // Launch misc functions
    hourInterval = setInterval(function(){
    	$(".hour").empty().append(new Date().toLocaleTimeString())
    	$(".day").empty().append(new Date().toLocaleDateString())
    }, 100)
    
    tableInterval = setInterval(function(){
        fillRecordsTable();
        toggleInOutButton();
        AmIworking();

    }, 100)

    populateForm();
    tgif();
}

/*=================================
=            Functions            =
=================================*/

function getInOutValue() {
    return JSON.parse(localStorage.getItem("project")).in_out;
}

function populateForm() {
    if (localStorage.getItem("project") && localStorage.getItem("project").in_out) {
        var formData = JSON.parse(localStorage.getItem("project"));
        $("#project option[value=" + formData.employee + "]").attr("selected", "selected");
    }
}

function deleteData(callback) {
    var selectedProject = $("#project option:selected").val();

    /* Get all records */
    allRecords = $.getAllRecords();

    /* Create new records object */
    newRecords = [];
    var i = 0;
    $.each(allRecords, function(i, v) {
        if (v.projectSubID != selectedProject) {
            newRecords.push(this)
            i++;
        } else if (selectedProject == "all"){
        	newRecords = newRecords
        }
    })

    newRecords = JSON.stringify(newRecords);

    var response = "";

    $.ajax({
        method: "POST",
        url: "/scripts/delete-project.php",
        async: false,
        data: {
            data: newRecords
        },
        success: function(data) {
            response = data
            callback();
        }
    })
}

function filterRecords(allRecords, key, value) {
    if (value != "all") {
        allRecords = $.grep(allRecords, function(n, i) {
            return n[key] == value;
        });
    }
    return allRecords;
}

function fillRecordsTable() {
    console.log("Fill")
    /* Collection forms values */
    var selectedProject = $("#project option:selected").val();
    var selectedEmployee = $("#name option:selected").val();

    /* Get all records (json) */
    allRecords = $.getAllRecords();
    /* Filter on employee name and projectID */
    allRecords = filterRecords(allRecords, "employee", $("#name option:selected").val())
    allRecords = filterRecords(allRecords, "projectSubID", $("#project option:selected").val())
    allRecords = filterRecords(allRecords, "type", $("#type-of-job option:selected").val())

    allRecords.reverse() // last one in first place

    /* Filling table */
    $(".projects-table tbody").empty();

    if (getInOutValue() == "out") {
        if (allRecords.length == 0) {
            $(".projects-table tbody").append('\
				<tr class="table-row danger">\
	    			<td colspan="8" class="text-center lead">No data for these parameters (try to F5)</td>\
				</tr>\
			')
        }
    }

    if (getInOutValue() == "in") {
        storageProject = JSON.parse(localStorage.getItem("project"));

        if (storageProject.time_out != "00000000000000"){
            out = new Date(storageProject.time_out).toLocaleTimeString()  
        } else {
            out = "---"
        }

        $(".projects-table tbody").append('\
			<tr class="table-row success">\
    			<td class="table-date">' + storageProject.time_in + '</td>\
    			<td class="table-in">' + storageProject.time_in + '</td>\
    			<td class="table-out">' + out + '</td>\
    			<td class="table-total">---</td>\
                <td class="table-employee">' + storageProject.employee + '</td>\
    			<td class="table-project"><i>' + storageProject.projectName + '</i> - ' + storageProject.projectSubName + '</td>\
    			<td class="table-type-of-work"><i>' + storageProject.type + '</td>\
    			<td class="table-desc">' + storageProject.description + '</td>\
			</tr>\
		')
    }
    $.each(allRecords, function(i, singleCheckingData) {
        time_in = fromLocaleString(singleCheckingData.date, singleCheckingData.time_in) 
        time_in = time_in.getTime();

        time_out = fromLocaleString(singleCheckingData.date, singleCheckingData.time_out) 
        time_out = time_out.getTime();

        time_diff = (time_out-time_in-3600000);

        time_diff_str = "";

        time_diff_str = new Date(time_diff).toLocaleTimeString();
    

        if (singleCheckingData.uniqID != undefined) {
            $(".projects-table tbody").append('\
				<tr class="table-row" data-uniqID="' + singleCheckingData.uniqID + '">\
	    			<td class="table-date editable" data-cell="date">' + singleCheckingData.date + '</td>\
                    <td class="table-in editable" data-cell="time_in">' + singleCheckingData.time_in + '</td>\
                    <td class="table-out editable" data-cell="time_out">' + singleCheckingData.time_out + '</td>\
	    			<td class="table-total" >' + time_diff_str + '</td>\
                    <td class="table-employee" data-cell="employee" >' + singleCheckingData.employee + '</td>\
	    			<td class="table-project" data-cell="projectSubName" ><i>' + singleCheckingData.projectName + '</i> - ' + singleCheckingData.projectSubName + '</td>\
    				<td class="table-type-of-work" data-cell="type-of-work"><i>' + singleCheckingData.type + '</td>\
	    			<td class="table-desc editable" data-cell="description" >' + singleCheckingData.description + '</td>\
				</tr>\
			')
        }
    })

}


/*=====  End of Functions  ======*/


/*=============================================
=            LocaStorage functions            =
=============================================*/

function LS_setProject(in_out) { // Set the LocalStorage with all data and check in/out


    var data_old = JSON.parse(localStorage.getItem("project")); // Store old LocalStorage
    // var current_in_out = getInOutValue(); // User is in or out?

    in_out = (typeof in_out === "undefined") ? getInOutValue() : in_out; // make in_out param optional

    // Collect form data
    var employee = $("#name").val();
    var projectSubID = $("#project").val();
    var projectName = $("#project option:selected").parent("optgroup").attr("label");
    var projectSubName = $("#project option:selected").data("name");
    var typeOfJob = $("#type-of-job").val();
    var desc = $("#desc").val();


    // Create uniqid with timestamp
    var uniqID = Date.now();

    var time = Date.now();

    if (in_out == "out") { // If user checks out
        // If user checks out, get the "in" time
        var check_in_time = data_old.time_in
        var check_out_time = new Date(time).toLocaleTimeString();
    } else if (in_out == "in") { // If user checks in
        // In user checks in, reset the "out" counter
        var check_out_time = "00000000000000";
        var check_in_time = new Date(time).toLocaleTimeString();
    }
    date = new Date(time).toLocaleDateString();

    var project_data = {
        "in_out": in_out,
        "uniqID": uniqID,
        "date": date,
        "time_in": check_in_time,
        "time_out": check_out_time,
        "employee": employee,
        "description": desc,
        "type": typeOfJob,
        "projectName": projectName,
        "projectSubID": projectSubID,
        "projectSubName": projectSubName
    }

    localStorage.setItem("project", JSON.stringify(project_data));
}

/*=====  End of LocaStorage functions  ======*/


/*==================================
=            AJAX calls            =
==================================*/

$.extend({
    getEmployees: function() {
        var result = null;

        $.ajax({
            url: "data/employees.json",
            type: 'get',
            dataType: 'json',
            async: false,
            success: function(data) {
                result = data;
            }
        });

        return result;
    }
})

$.extend({
    getProjects: function() {
        var result = null;

        $.ajax({
            url: "data/projects.json",
            type: 'get',
            dataType: 'json',
            async: false,
            success: function(data) {
                result = data;
            }
        });

        return result;
    }
})

$.extend({
    getTypeOfJob: function() {
        var result = null;

        $.ajax({
            url: "data/type-of-job.json",
            type: 'get',
            dataType: 'json',
            async: false,
            success: function(data) {
                result = data;
            }
        });

        return result;
    }
})

$.extend({
    getAllRecords: function() {
        // local var
        var theResponse = null;
        // jQuery ajax
        $.ajax({
            url: "data/projects-data.json",
            type: 'get',
            dataType: "json",
            async: false,
            success: function(respText) {
                theResponse = respText;
            }
        });
        // Return the response text
        return theResponse.projects;
    }
})

$.extend({
    saveLastCheckOut: function() {
        $("#check-out").removeClass("btn-danger").addClass("btn-disabled").empty().append("<span class='fa fa-spinner fa-spin'></span>")
        $.ajax({
            method: "POST",
            url: "/scripts/save-last-checkout.php",
            async: false,
            data: {
                addCheckOut: localStorage.getItem("project")
            },
            success: function(data) {
                setTimeout(function() {
                    $("#check-out").empty().append("Check Out")
                }, 750)

            }
        })
    }
})

$.extend({
	editData: function(id, data, cell){

	    jsonObj = $.getAllRecords();
	    for (var i = 0; i < jsonObj.length; i++) {
	        if (jsonObj[i].uniqID === id) {

	            jsonObj[i][cell] = data;
	            var response = "";
	            $.ajax({
	                method: "POST",
	                url: "/scripts/update-project.php",
	                async: false,
	                data: {
	                    data: JSON.stringify(jsonObj)
	                },
	                success: function(data) {
	                    response = data
	                }
	            })
	        }
	    }
	}
}) 

/*=====  End of AJAX calls  ======*/


/*======================================
=            Misc functions            =
======================================*/

function toggleInOutButton() {

    if (getInOutValue() == "in") {
        $("#check-in").addClass("btn-disabled").removeClass("btn-success")

        $("#check-out").addClass("btn-danger").removeClass("btn-disabled")

    } else {
        $("#check-in").addClass("btn-success").removeClass("btn-disabled")

        $("#check-out").addClass("btn-disabled").removeClass("btn-danger")

    }
}

function page(data) {
    if ($(".page").data("page") == data) {
        return true;
    } else {
        return false;
    }
}

function AmIworking() {

    if (localStorage.getItem("project")) {
        var project_data = JSON.parse(localStorage.getItem("project"));
        if (project_data.in_out == "in") {
            $(".working").empty().append("Yes")
        } else {
            $(".working").empty().append("No")
        }
    }
}

function tgif() {
    day = new Date().getDay();
    if ((day > 0) && (day < 6)) {
        $(".tgif").empty().append(-(day - 6) + " days before the weekend!");
    } else if (day == 6) {
        $(".tgif").empty().append("Close this shit, it's the weekend, enjoy your hangover!");
    } else if (day == 0) {
        $(".tgif").empty().append("Close this shit, and go for a nap, you have to work tomorrow!");
    }
}

/*=====  End of Misc functions  ======*/


/*=========================================
=            Usefull functions            =
=========================================*/


function JSONToCSV(JSONData, ReportTitle, ShowLabel, projectName) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    var CSV = '';
    //Set Report title in first row or line

    CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";

        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {

            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);

        //append Label row with line break
        CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);

        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {
        alert("Invalid data");
        return;
    }

    //Generate a file name
    var fileName = "Report_" + projectName + "_";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g, "_");

    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    

    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");
    link.href = uri;

    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}


/*=====  End of Usefull functions  ======*/


$.fn.makeEditable = function() {
  $(this).on('click',function(){
    if($(this).find('input').is(':focus')) return this;
    var cell = $(this);
    var content = $(this).html();
    $(this).html('<input type="text" value="' + $(this).html() + '" />')
      .find('input')
      .trigger('focus')
      .on({
        'blur': function(){
          $(this).trigger('saveEditable');
        },
        'keyup':function(e){
          if(e.which == '13'){ // enter
            $(this).trigger('saveEditable');
          } else if(e.which == '27'){ // escape
            $(this).trigger('closeEditable');
          }
        },
        'closeEditable':function(){
          cell.html(content);
        },
        'saveEditable':function(){
          content = $(this).val();
          $.editData($(this).parent().parent().data("uniqid"), content, $(this).parent().data("cell"))

          $(this).trigger('closeEditable');
        }
    });
  });
return this;
}

function fromLocaleString(date, time){
    date = date.split("/");
    time = time.split(":");
    timedate = new Date(date[2],date[1], date[0], time[0], time[1], time[2])
    return timedate;
}