# It's on the clock! #

It's a simple employee time clock. Select the user, check in and check out.

# Features #
* Check in / check out records
* Edit records
* Delete records
* Export to CSV
* Add infinite employees (JSON edit)
* Add infinite projects (JSON edit)

# Add a project #

To add a project, simply add it to the "projects. json" file following the right syntax.
Attention, the main ID should coincide with the subID!
A project SHOULD have a subproject!

# Add a user #
To add a user, add it to the "employees.json" file following the right syntax.

# What's project-data.json? #
This file contains all records, you can edit it as your own risks!

# Data page #
The Data page allows you to export CSV, delete and edit project data!

#Yup, it's messy!"#
Don't mind this messy project! I will clean it soon! Sorry for these commit messages and this approximative english.